#!/usr/bin/env bash

set -e

source ./common.sh

function dev_frontend() {
  prefix_stdout_stderr "${PURPLE}frontend: ${NC}"

  cd ./frontend

  npm run dev
}

function dev_backend() {
  prefix_stdout_stderr "${CYAN}backend:  ${NC}"

  cd ./backend
  source venv/bin/activate

  python main.py
}

run_frontend_backend "dev_frontend" "dev_backend"
