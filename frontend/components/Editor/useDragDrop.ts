import { useEffect, useState } from "react";

type TUseDragDrop = () => { displayDragDrop: boolean };

export const useDragDrop: TUseDragDrop = () => {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(process.browser);
  }, []);

  return { displayDragDrop: isBrowser };
};
