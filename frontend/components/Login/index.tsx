import React, { useState } from "react";
import { useAuth } from "components/Login/AuthContext";
import Head from "next/head";

const LoginBox: React.FC = () => {
  const [password, setPassword] = useState("");
  const [focused, setFocused] = useState(false);
  const [loginFailed, setLoginFailed] = useState(false);
  const { loggedIn, login } = useAuth();

  const passwordLabelClassName = `absolute inset-y-0 left-0 px-4 font-sans text-gray-600 ${
    focused || password
      ? "transform scale-75 -translate-y-5 -translate-x-2"
      : ""
  } transition-transform pointer-events-none`;

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();

    if (!loggedIn) {
      const loginSuccessful = await login(password);

      if (!loginSuccessful) {
        setLoginFailed(true);
        setPassword("");
      }
    }
  };

  return (
    <div className="fixed h-screen w-full overflow-auto bg-gray-100">
      <div className="m-auto h-full flex justify-center items-center">
        <div className="container m-auto h-auto flex flex-col justify-center items-center p-10 space-y-20">
          <Head>
            <title>Login</title>
          </Head>
          <div className="flex flex-col justify-center items-center space-y-10">
            <div className="flex flex-row justify-center items-center space-x-5">
              <img
                src="images/csc-logo-trans.png"
                height={80}
                width={80}
                alt="CSC Logo"
              />
              <h1 className="text-4xl font-sans font-bold text-gray-900">
                linklist
              </h1>
            </div>
            <h2 className="text-xl font-sans font-semibold text-gray-900 text-center">
              Log in to continue to your Linklist admin
            </h2>
          </div>
          <div className="flex justify-center items-center px-10 py-8 bg-gray-50 border-2 border-gray-300 rounded-lg">
            <div className="space-y-4">
              {loginFailed ? (
                <div className="text-red-600">Invalid credentials.</div>
              ) : null}
              <form onSubmit={handleSubmit} className="space-y-6">
                <div>
                  <label htmlFor="password" className="relative">
                    <span className={passwordLabelClassName}>Password</span>
                  </label>
                  <input
                    name="password"
                    type="password"
                    value={password}
                    onFocus={() => setFocused(true)}
                    onBlur={() => {
                      setFocused(false);
                      setLoginFailed(false);
                    }}
                    onChange={(event) => setPassword(event.target.value)}
                    className="bg-transparent p-4 border border-gray-300 leading-snug focus:outline-none focus:border-gray-500 rounded"
                  />
                </div>
                <input
                  type="submit"
                  value="Log In"
                  className="w-full px-4 py-2 font-sans font-semibold text-white bg-purple-700 focus:outline-none focus:ring-4 focus:ring-purple-300 rounded-lg"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginBox;
